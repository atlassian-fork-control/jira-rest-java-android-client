package com.atlassian.jira.rest.client.internal.json;

import com.google.gson.JsonElement;

public interface JsonElementParser<T> extends JsonParser<JsonElement, T> {
}
